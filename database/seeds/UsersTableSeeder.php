<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
           'name' => 'Ayman Medhat',
           'email' => 'kashmos@outlook.com',
           'password' => bcrypt('KashmosSystem#1'),
        ]);
    }
}
